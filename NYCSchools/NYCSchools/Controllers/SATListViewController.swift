//
//  SATListViewController.swift
//  NYCSchools
//
//  Created by vivek on 15/03/23.
//
import Foundation
import UIKit

class SATListViewController: UIViewController, UITableViewDelegate {
    
    var isSchool : Bool = false
    
    @IBOutlet weak var SATListTableView: UITableView!
    
    private var satViewModel : SATViewModel!
    
    private var dataSource : SATDataSource<SatDetailTableViewCell,SatData>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "SAT Data"
        callToViewModelForUIUpdate()
    }
    
    func callToViewModelForUIUpdate(){
        
        self.satViewModel =  SATViewModel()
        self.satViewModel.bindSATViewModelToController = {
            self.updateDataSource()
        }
    }
    
    func updateDataSource(){
        self.dataSource = SATDataSource(cellIdentifier: "SatDetailTableViewCell", items: self.satViewModel.satData, configureCell: { (cell, sat) in
            cell.numSeatsLabel.text = sat.numTestTakers
            cell.readingAvg.text = sat.readingAvg
            cell.writingAvg.text = sat.writingAvg
            cell.mathsAvg.text = sat.mathAvg
            cell.schoolNameLabel.text = sat.schoolName
        })
        
        DispatchQueue.main.async {
            self.SATListTableView.dataSource = self.dataSource
            self.SATListTableView.reloadData()
        }
    }
    
   
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let schoolInfo = self.satViewModel.satData[indexPath.row]
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let schoolDetailVC : SchoolDetailsViewController = storyboard.instantiateViewController(withIdentifier: "schoolDetailView") as! SchoolDetailsViewController
//        schoolDetailVC.schoolInfo = schoolInfo
//        self.navigationController?.pushViewController(schoolDetailVC, animated: true)
//    }
    
}

