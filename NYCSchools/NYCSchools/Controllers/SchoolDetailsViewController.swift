//
//  SchoolDetailsViewController.swift
//  NYCSchools
//
//  Created by vivek on 14/03/23.
//

import Foundation
import UIKit

class SchoolDetailsViewController: UIViewController {
    
    var schoolInfo : SchoolData!
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var schoolOverView: UITextView!
    @IBOutlet weak var schoolEmail: UILabel!
    @IBOutlet weak var schoolWebsite: UILabel!
    
    @IBOutlet weak var phoneNumberTxt: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.renderData()
    }
    
    @IBAction func ViewSATDataForSchool(_ sender: Any) {
    }
    @IBAction func viewSATDetails(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let satListVC : SATListViewController = storyboard.instantiateViewController(withIdentifier: "satlistview") as! SATListViewController
        self.navigationController?.pushViewController(satListVC, animated: true)
    }
    func renderData() {
        
        self.schoolNameLabel.text = schoolInfo.schoolName
        self.schoolOverView.text = schoolInfo.overviewParagraph
        self.schoolEmail.text = schoolInfo.schoolEmail
        self.schoolWebsite.text = schoolInfo.schoolWebsite
        self.phoneNumberTxt.text = schoolInfo.phoneNumber

    }
}
