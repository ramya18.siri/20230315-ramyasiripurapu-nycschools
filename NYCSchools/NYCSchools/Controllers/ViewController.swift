//
//  HomeViewController.swift
//  NYCSchools
//
//  Created by vivek on 14/03/23.
//

import Foundation

//

import UIKit

class ViewController: UIViewController, UITableViewDelegate {
    
    
    @IBOutlet weak var schooldisplayTableView: UITableView!
    
    private var schoolViewModel : SchoolViewModel!
    
    private var dataSource : SchoolDataSource<SchoolTableViewCell,SchoolData>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "NYC Schools"
        self.schooldisplayTableView.delegate = self
        callToViewModelForUIUpdate()
    }
    
    func callToViewModelForUIUpdate(){
        
        self.schoolViewModel =  SchoolViewModel()
        self.schoolViewModel.bindSchoolViewModelToController = {
            self.updateDataSource()
        }
    }
    
    func updateDataSource(){
        
        self.dataSource = SchoolDataSource(cellIdentifier: "SchoolTableViewCell", items: self.schoolViewModel.schoolData, configureCell: { (cell, evm) in
            cell.schoolDBN.text = evm.schoolName
            cell.schoolNameLabel.text = evm.phoneNumber
        })
        
        DispatchQueue.main.async {
            self.schooldisplayTableView.dataSource = self.dataSource
            self.schooldisplayTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let schoolInfo = self.schoolViewModel.schoolData[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let schoolDetailVC : SchoolDetailsViewController = storyboard.instantiateViewController(withIdentifier: "schoolDetailView") as! SchoolDetailsViewController
        schoolDetailVC.schoolInfo = schoolInfo
        self.navigationController?.pushViewController(schoolDetailVC, animated: true)
    }
    
}

