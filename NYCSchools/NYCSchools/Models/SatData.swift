//
//  SatData.swift
//  NYCSchools
//
//  Created by vivek on 15/03/23.
//

import Foundation

// MARK: - School data
struct SatData: Decodable {
    
    let schoolName : String
    let dbn: String
    let numTestTakers: String
    let readingAvg: String
    let mathAvg: String
    let writingAvg: String

    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case dbn = "dbn"
        case numTestTakers = "num_of_sat_test_takers"
        case readingAvg = "sat_critical_reading_avg_score"
        case mathAvg = "sat_math_avg_score"
        case writingAvg = "sat_writing_avg_score"
        
        
        
        }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.schoolName = try values.decodeIfPresent(String.self, forKey: .schoolName) ?? ""
        self.dbn = try values.decodeIfPresent(String.self, forKey: .dbn) ?? ""
        self.readingAvg = try values.decodeIfPresent(String.self, forKey: .readingAvg) ?? ""
        self.mathAvg = try values.decodeIfPresent(String.self, forKey: .mathAvg) ?? ""
        self.writingAvg = try values.decodeIfPresent(String.self, forKey: .writingAvg) ?? ""
        self.numTestTakers = try values.decodeIfPresent(String.self, forKey: .numTestTakers) ?? ""


    }
}
