//
//  SchoolData.swift//
//  Created by Ramya on 14/03/23.
//

import Foundation
// MARK: - School data
struct SchoolData: Decodable {
    
    let schoolName : String
    let dbn: String
    let nta: String

    let boro: String
    let overviewParagraph: String
    let schoolTenthSeats: String
    let academicOpportunities1: String
    let academicOpportunities2: String
    let ellPrograms: String
    let neighborhood: String
    let buildingCode: String
    let location: String
    let phoneNumber: String
    let faxNumber: String
    let schoolEmail: String
    let schoolWebsite: String
    let subway:String
    let bus: String
    let grades2018: String
    let finalgrades: String
    let total_students:String
    let extracurricular_activities: String
    let school_sports: String
    let requirement1_1: String
    let requirement2_1: String
    let requirement3_1: String
    let requirement4_1: String
    let requirement5_1: String
    let offer_rate1: String
    let program1: String
    let code1: String
    let interest1:String
    let admissionspriority11: String
    let admissionspriority21: String
    let admissionspriority31: String
    let grade9geApplicantsperseat1: String
    let grade9swdApplicantsperseat1: String
    let primary_address_line_1: String
    let city: String
    let zipCode : String
    let stateCode: String
    let latitude: String
    let longitude: String
    let borough: String
    
    
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case dbn = "dbn"
        case boro = "boro"
        case overviewParagraph = "overview_paragraph"
        case schoolTenthSeats = "school_10th_seats"
        case academicOpportunities1 = "academicopportunities1"
        case academicOpportunities2 = "academicopportunities2"
        case ellPrograms = "ell_programs"
        case neighborhood = "neighborhood"
        case buildingCode = "building_code"
        case location = "location"
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case schoolWebsite = "website"
        case subway = "subway"
        case bus = "bus"
        case grades2018 = "grades2018"
        case finalgrades = "finalgrades"
        case total_students = "total_students"
        case extracurricular_activities = "extracurricular_activities"
        case school_sports = "school_sports"
        case requirement1_1 = "requirement1_1"
        case requirement2_1 = "requirement2_1"
        case requirement3_1 = "requirement3_1"
        case requirement4_1 = "requirement4_1"
        case requirement5_1 = "requirement5_1"
        case offer_rate1 = "offer_rate1"
        case program1 = "program1"
        case code1 = "code1"
        case interest1 = "interest1"
        case admissionspriority11 =  "admissionspriority11"
        case admissionspriority21 =  "admissionspriority21"
        case admissionspriority31 = "admissionspriority31"
        case grade9geApplicantsperseat1 =  "grade9geapplicantsperseat1"
        case grade9swdApplicantsperseat1 = "grade9swdapplicantsperseat1"
        case primary_address_line_1 = "primary_address_line_1"
        case city = "city"
        case zipCode = "zip"
        case stateCode = "state_code"
        case latitude = "latitude"
        case longitude = "longitude"
        case nta = "nta"
        case borough = "borough"
        
        
        }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.schoolName = try values.decodeIfPresent(String.self, forKey: .schoolName) ?? ""
        self.dbn = try values.decodeIfPresent(String.self, forKey: .dbn) ?? ""
        self.nta = try values.decodeIfPresent(String.self, forKey: .nta) ?? ""
        self.boro = try values.decodeIfPresent(String.self, forKey: .boro) ?? ""
        self.overviewParagraph = try values.decodeIfPresent(String.self, forKey: .overviewParagraph) ?? ""
        self.schoolTenthSeats = try values.decodeIfPresent(String.self, forKey: .schoolTenthSeats) ?? ""
        self.academicOpportunities1 = try values.decodeIfPresent(String.self, forKey: .academicOpportunities1) ?? ""
        self.academicOpportunities2 = try values.decodeIfPresent(String.self, forKey: .academicOpportunities2) ?? ""
        self.ellPrograms = try values.decodeIfPresent(String.self, forKey: .ellPrograms) ?? ""
        self.neighborhood = try values.decodeIfPresent(String.self, forKey: .neighborhood) ?? ""
        self.buildingCode = try values.decodeIfPresent(String.self, forKey: .buildingCode) ?? ""
        self.location = try values.decodeIfPresent(String.self, forKey: .location) ?? ""
        self.phoneNumber = try values.decodeIfPresent(String.self, forKey: .phoneNumber) ?? ""
        self.faxNumber = try values.decodeIfPresent(String.self, forKey: .faxNumber) ?? ""
        self.schoolEmail = try values.decodeIfPresent(String.self, forKey: .schoolEmail) ?? ""
        self.schoolWebsite = try values.decodeIfPresent(String.self, forKey: .schoolWebsite) ?? ""
        self.subway = try values.decodeIfPresent(String.self, forKey: .subway) ?? ""
        self.bus = try values.decodeIfPresent(String.self, forKey: .bus) ?? ""
        self.grades2018 = try values.decodeIfPresent(String.self, forKey: .grades2018) ?? ""
        self.finalgrades = try values.decodeIfPresent(String.self, forKey: .finalgrades) ?? ""
        self.total_students = try values.decodeIfPresent(String.self, forKey: .total_students) ?? ""
        self.extracurricular_activities = try values.decodeIfPresent(String.self, forKey: .extracurricular_activities) ?? ""
        self.school_sports = try values.decodeIfPresent(String.self, forKey: .school_sports) ?? ""
        self.requirement1_1 = try values.decodeIfPresent(String.self, forKey: .requirement1_1) ?? ""
        self.requirement2_1 = try values.decodeIfPresent(String.self, forKey: .requirement2_1) ?? ""
        self.requirement3_1 = try values.decodeIfPresent(String.self, forKey: .requirement3_1) ?? ""
        self.requirement4_1 = try values.decodeIfPresent(String.self, forKey: .requirement4_1) ?? ""
        self.requirement5_1 = try values.decodeIfPresent(String.self, forKey: .requirement5_1) ?? ""
        self.offer_rate1 = try values.decodeIfPresent(String.self, forKey: .offer_rate1) ?? ""
        self.program1 = try values.decodeIfPresent(String.self, forKey: .program1) ?? ""
        self.code1 = try values.decodeIfPresent(String.self, forKey: .code1) ?? ""

        self.interest1 = try values.decodeIfPresent(String.self, forKey: .interest1) ?? ""
        self.admissionspriority11 = try values.decodeIfPresent(String.self, forKey: .admissionspriority11) ?? ""
        self.admissionspriority21 = try values.decodeIfPresent(String.self, forKey: .admissionspriority21) ?? ""
        self.admissionspriority31 = try values.decodeIfPresent(String.self, forKey: .admissionspriority31) ?? ""
        self.grade9geApplicantsperseat1 = try values.decodeIfPresent(String.self, forKey: .grade9geApplicantsperseat1) ?? ""
        self.grade9swdApplicantsperseat1 = try values.decodeIfPresent(String.self, forKey: .grade9swdApplicantsperseat1) ?? ""
        self.primary_address_line_1 = try values.decodeIfPresent(String.self, forKey: .primary_address_line_1) ?? ""
        self.city = try values.decodeIfPresent(String.self, forKey: .city) ?? ""
        self.zipCode = try values.decodeIfPresent(String.self, forKey: .zipCode) ?? ""
        self.latitude = try values.decodeIfPresent(String.self, forKey: .latitude) ?? ""
        self.longitude = try values.decodeIfPresent(String.self, forKey: .longitude) ?? ""
        self.borough = try values.decodeIfPresent(String.self, forKey: .borough) ?? ""
        self.stateCode = try values.decodeIfPresent(String.self, forKey: .stateCode) ?? ""

    }
}
