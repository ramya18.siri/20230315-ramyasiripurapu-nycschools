//
//  SatDetailTableViewCell.swift
//  NYCSchools
//
//  Created by vivek on 15/03/23.
//

import Foundation

import UIKit

class SatDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var schoolNameLabel: UILabel!
    
    @IBOutlet weak var numSeatsLabel: UILabel!
    
    @IBOutlet weak var readingAvg: UILabel!
    
    @IBOutlet weak var writingAvg: UILabel!
    @IBOutlet weak var mathsAvg: UILabel!
    
    
    var satData : SatData? {
        didSet {
            schoolNameLabel.text = satData?.schoolName
            numSeatsLabel.text = satData?.numTestTakers
            readingAvg.text = satData?.readingAvg
            writingAvg.text = satData?.writingAvg
            mathsAvg.text = satData?.mathAvg
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
