//
//  SATViewModel.swift
//  NYCSchools
//
//  Created by vivek on 15/03/23.
//

import Foundation

class SATViewModel : NSObject {
    
    private var apiService : APIService!
    private(set) var satData : [SatData]! {
        didSet {
            self.bindSATViewModelToController()
        }
    }
    
    var bindSATViewModelToController : (() -> ()) = {}
    
    override init() {
        super.init()
        self.apiService =  APIService()
        callFuncToGetSATData()
    }
    
    func callFuncToGetSATData() {
        self.apiService.apiToGetSATData { (satData) in
            self.satData = satData
        }
    }
}
