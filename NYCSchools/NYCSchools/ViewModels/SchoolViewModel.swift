
//  SchoolViewModel.swift
//Created by Ramya on 14/03/23.

import Foundation

class SchoolViewModel : NSObject {
    
    private var apiService : APIService!
    private(set) var schoolData : [SchoolData]! {
        didSet {
            self.bindSchoolViewModelToController()
        }
    }
    
    var bindSchoolViewModelToController : (() -> ()) = {}
    
    override init() {
        super.init()
        self.apiService =  APIService()
        callFuncToGetSchoolData()
    }
    
    func callFuncToGetSchoolData() {
        self.apiService.apiToGetSchoolData { (schoolData) in
            self.schoolData = schoolData
        }
    }
}
